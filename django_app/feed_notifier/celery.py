import os
from celery import Celery


os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'feed_notifier.settings')

app = Celery('feed_notifier')

app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks()


@app.task(bind=True)
def debug_taks(self):
    print('Request: {0!r}'.format(self.request))
