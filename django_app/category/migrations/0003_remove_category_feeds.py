# Generated by Django 2.1.1 on 2018-09-24 12:04

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('category', '0002_category_feeds'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='category',
            name='feeds',
        ),
    ]
