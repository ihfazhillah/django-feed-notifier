from django.urls import path
from . import views

urlpatterns = [
    path('', views.CategoryListView.as_view(), name='list'),
    path('create', views.CategoryCreateView.as_view(), name='add'),
    path('<int:pk>/edit', views.CategoryUpdateView.as_view(), name='edit'),
]
