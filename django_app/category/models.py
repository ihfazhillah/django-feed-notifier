from django.db import models
from django.urls import reverse_lazy

# Create your models here.
class Category(models.Model):
    name = models.CharField(max_length=200)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        pass
        # return reverse_lazy('category:detail', args=[self.id])
