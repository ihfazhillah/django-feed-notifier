from django.shortcuts import render
from django.views.generic import ListView, CreateView, UpdateView
from .models import Category
from django.urls import reverse_lazy

# Create your views here.

class CategoryListView(ListView):
    model = Category
    template_name = 'category/list.html'

class CategoryCreateView(CreateView):
    model = Category
    template_name = 'category/manipulate.html'
    fields = ['name']
    success_url = reverse_lazy('category:list')

class CategoryUpdateView(UpdateView):
    model = Category
    template_name = 'category/manipulate.html'
    fields = ['name']
    success_url = reverse_lazy('category:list')

# https://stackoverflow.com/questions/3011179/django-the-included-urlconf-doesnt-have-any-patterns-in-it
