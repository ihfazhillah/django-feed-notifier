from django.urls import path
from . import views

urlpatterns = [
    path('', views.FeedListView.as_view(), name='list'),
    path('add', views.FeedCreateView.as_view(), name='add'),
    path('<int:pk>', views.FeedDetailView.as_view(), name='detail'),
    path('<int:pk>/edit', views.FeedEditView.as_view(), name='edit')
]

