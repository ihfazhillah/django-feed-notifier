from django.db import models
from django.urls import reverse
from category.models import Category

# Create your models here.

class Feed(models.Model):
    title = models.CharField(max_length=200)
    url = models.URLField()
    description = models.TextField(blank=True, null=True)
    category = models.ForeignKey(Category,
                                 on_delete=models.CASCADE,
                                 related_name='feeds')

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('feeds:detail', args=[self.id])
