from celery import shared_task
import feedparser
import redis
import pickle

r = redis.StrictRedis(host='redis',
                      password='devpassword')

@shared_task
def get_feed(url, no_cache=False):
    key = 'feed_response:{}'.format(url)

    if no_cache or not r.exists(key):
        print("getting data from internet")
        print("{}".format(url))
        print("-"*30)
        response = feedparser.parse(url)
        r.set(key, pickle.dumps(response))
        return response

    print("getting data from cache")
    print("{}".format(url))
    print("-"*30)
    return pickle.loads(r.get(key))



