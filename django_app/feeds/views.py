from django.shortcuts import render
from django.views.generic import ListView, CreateView, DetailView, UpdateView
from django.urls import reverse
from .models import Feed

# Create your views here.

class FeedListView(ListView):
    model = Feed
    template_name = 'feeds/list.html'

class FeedCreateView(CreateView):
    model = Feed
    template_name = 'feeds/add.html'
    fields = ['title', 'url', 'description', 'category']

class FeedDetailView(DetailView):
    model = Feed
    template_name = 'feeds/detail.html'
    
    def get_context_data(self, **kwargs):
        context = super(FeedDetailView, self).get_context_data(**kwargs)
        context['props'] = {
            'id': self.object.id,
            'url': reverse('api:create-job')
        }
        return context

class FeedEditView(UpdateView):
    model = Feed
    template_name = 'feeds/add.html'
    fields = ['title', 'url', 'description', 'category']
