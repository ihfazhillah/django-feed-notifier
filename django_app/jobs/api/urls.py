from django.urls import path
from . import views


urlpatterns = [
    path('job/', views.create_job, name='create-job'),
    path('job/<id>', views.check_job, name='check-job')
]
