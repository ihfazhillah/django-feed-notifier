from django.shortcuts import get_object_or_404
from django.http import JsonResponse
from django.urls import reverse
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_POST
from rest_framework.parsers import JSONParser
from rest_framework.renderers import JSONRenderer
from rest_framework.decorators import api_view
from rest_framework.response import Response
from celery.result import AsyncResult 
from .serializers import JobCreateSerializer

from feeds.tasks import get_feed
from feeds.models import Feed


accepted_jobs = {
    'get_feed': get_feed
}

@csrf_exempt
@api_view(['POST'])
def create_job(request):
    data = JSONParser().parse(request)
    serialized = JobCreateSerializer(data=data)
    if serialized.is_valid():

        job_type = accepted_jobs.get(serialized.data['type'])

        if job_type:
            feed = get_object_or_404(Feed, pk=serialized.data.get('id'))

            task = getattr(job_type, 'delay')(feed.url, no_cache=serialized.data.get('no_cache'))

            result = {
                'url': reverse('api:check-job', args=[task.id])
            }

            return Response(result, status=200)
    return Response({'error': True}, status=400)


@api_view(['GET'])
def check_job(request, id):
    task = AsyncResult(id)

    result = {
        'status': task.status,
        'result': task.result
    }

    return Response(result, status=200)

