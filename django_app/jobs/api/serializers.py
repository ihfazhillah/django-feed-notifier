from rest_framework import serializers


class JobCreateSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    type = serializers.CharField()
    no_cache = serializers.BooleanField(default=False)
