import React from 'react'
import axios from 'axios'

export default class Preview extends React.Component {
	constructor(props){
		super(props)

		this.state = {
			loading: false,
			result: {}
		}

		this.onGetFeed = this.onGetFeed.bind(this)
		this.checkInterval = this.checkInterval.bind(this)
	}

	onGetFeed(e){
		const { id, url } = this.props
		this.setState({loading: true})
		axios.post(url, {
			type: 'get_feed',
			id: id
		}).then((response) => {
			this.checkInterval(response.data.url)
		}).catch((error) => {
			this.setState({loading: false})
		})

	}

	checkInterval(url){
		console.log(url)
		this.interval = setInterval(() => {
			axios.get(url).then(response => {
				if (response.data.status === 'SUCCESS') {
					clearInterval(this.interval)
					this.setState({result: response.data.result, loading: false})
				}

			}).catch(error => {console.log(error); this.setState({loading: false})})
		}, 1000)

	}

	render(){
		return <div>
			{!this.state.loading && <button className="btn btn-primary mx-auto" onClick={this.onGetFeed}>Get Feed</button>}
			{this.state.loading && <span className="fa fa-spin fa-spinner"></span>}

			<p></p>
			<p className='lead'>Entries</p>
			{this.state.result.entries && 
					<ul className="list-group">
						{this.state.result.entries.map((entry, index) => (
							<li key={index} className="list-group-item"><a href={ entry.link } target="_blank">{ entry.title }</a></li>
						))}
					</ul>
			}
			</div>
	}
}
