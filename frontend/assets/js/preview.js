import React from 'react'
import ReactDOM from 'react-dom'
import Preview from './components/Preview.js'

ReactDOM.render(<Preview {...window.previewProps}/>,
  document.getElementById('preview-id'))
