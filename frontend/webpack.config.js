var path = require('path')
var webpack = require('webpack')
var BundleTracker = require('webpack-bundle-tracker')


module.exports = {
	context: __dirname,

  entry: {
    main: './assets/js/index.js',
    preview: './assets/js/preview.js'
  },

	output: {
		path: path.resolve('./assets/bundles/'),
		filename: '[name]-[hash].js'
	},

  mode: 'development',

	plugins: [
		new BundleTracker({filename: 'webpack-stats.json'}),
	],

	module: {
		rules: [
			{test: /\.jsx?$/, exclude: /node_modules/, use: {loader: 'babel-loader'}}
		]
	},

	resolve: {
		modules: [path.resolve(__dirname, 'node_modules')],
		extensions: ['*', '.js', '.jsx']
	}


}
